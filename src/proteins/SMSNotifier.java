/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proteins;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wamae Benson
 */
public class SMSNotifier implements Notifier {

    private String userName;
    private String password;
    private String numberToMessage;

    public SMSNotifier(String userName, String password, String numberToMessage) {
        this.userName = userName;
        this.password = password;
        this.numberToMessage = numberToMessage;
    }

    @Override
    public boolean send(String message) {
        try {

            Voice voice = new Voice(userName, password);
            voice.sendSMS(numberToMessage, message);

        } catch (IOException e) {
            return false;
        }
        
        return true;
    }

}
