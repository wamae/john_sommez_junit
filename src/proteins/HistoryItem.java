/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proteins;

/**
 *
 * @author Wamae
 */
public class HistoryItem {

    private int historyId;
    private int amount;
    private String action;
    private int total;

    public HistoryItem(int historyId, int amount, String action, int total) {
        this.historyId = historyId;
        this.amount = amount;
        this.action = action;
        this.total = total;
    }

    public int getHistoryId() {
        return historyId;
    }

    public void setHistoryId(int historyId) {
        this.historyId = historyId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
    

    
    
    
    
}
