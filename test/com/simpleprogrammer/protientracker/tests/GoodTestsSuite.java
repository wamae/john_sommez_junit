/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpleprogrammer.protientracker.tests;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Wamae
 */
@RunWith(Categories.class)
@ExcludeCategory(BadTestsCategory.class)
@IncludeCategory(GoodTestsCategory.class)
@Suite.SuiteClasses({
    TrackingServiceTest.class,
    HelloJUnitTest.class
})
public class GoodTestsSuite {
    
}
