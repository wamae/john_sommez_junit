package com.simpleprogrammer.protientracker.tests;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.experimental.categories.Category;
import static org.hamcrest.CoreMatchers.*;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;
import proteins.HistoryItem;
import proteins.InvalidGoalException;
import proteins.Notifier;
import proteins.NotifierStub;
import proteins.TrackingService;

/**
 *
 * @author Wamae
 */
public class TrackingServiceTest {

    private TrackingService service;

    public TrackingServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("BeforeClass");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("AfterClass");
    }

    @Before
    public void setUp() {
        System.out.println("Before");
        service = new TrackingService(new NotifierStub());
    }

    @After
    public void tearDown() {
        System.out.println("After");
    }

    @Test
    @Category({GoodTestsCategory.class, BadTestsCategory.class})
    public void newTrackingServiceTotalsIsZero() {
        assertEquals("Tracking service total was zero", 0, service.getTotal());
    }

    @Test
    @Category(GoodTestsCategory.class)
    public void whenAddingProteinProteinIncreaseByAmount() {
        service.addProtein(10);
        assertEquals("Protein amount was not correct", 10, service.getTotal());
        assertThat(service.getTotal(), is(10));

        //assertThat(service.getTotal(),allOf(is(10)),instanceOf(Integer.class));
        //TODO: Error
    }

    @Test
    @Category(GoodTestsCategory.class)
    public void whenRemovingProteinTotalRemainsZero() {
        service.removeProtein(10);
        assertEquals(0, service.getTotal());
    }

    @Test
    @Ignore
    public void printNotIgnored() {
        fail("not ignored");
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //@Test(expected = InvalidGoalException.class)
    @Test
    public void whenGoalIsLessThanZeroExceptionIsThrown() throws InvalidGoalException {
        thrown.expect(InvalidGoalException.class);
        thrown.expectMessage("Goal was less than zero!");
        thrown.expectMessage(containsString("Goal"));
        service.setGoal(-5);
    }

    //@Rule
    //public Timeout timeout = new Timeout(5);
    //@Test(timeout = 10)
    public void badTest() {
        for (int i = 0; i < 1000000; i++) {
            service.addProtein(1);
        }
    }

    @Test
    public void whenGoalIsMetHistoryIsUpdated() throws InvalidGoalException {
        //TODO: Error
        Mockery context = new Mockery();
        final Notifier mockNotifier = context.mock(Notifier.class);

        service = new TrackingService(mockNotifier);

        context.checking(new Expectations() {
            {
                oneOf(mockNotifier.send("goal met"));
                will(returnValue(true));
            }
        });

        service.setGoal(5);
        service.addProtein(6);

        HistoryItem result = service.getHistory().get(1);
        assertEquals("sent:goal met", result.getAction());
        
        context.assertIsSatisfied();
    }

}
