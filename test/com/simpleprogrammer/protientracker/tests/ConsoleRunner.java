/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpleprogrammer.protientracker.tests;

import org.junit.runner.JUnitCore;

/**
 *
 * @author Wamae Benson
 */
public class ConsoleRunner {
    public static void main(String args[]){
        JUnitCore junit = new JUnitCore();
        junit.addListener(new org.junit.internal.TextListener(System.out));
        
        junit.run(TrackingServiceTest.class);
    }
}
