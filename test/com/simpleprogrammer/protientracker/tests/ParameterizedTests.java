/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpleprogrammer.protientracker.tests;

import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import proteins.TrackingService;
import static org.junit.Assert.*;
import proteins.NotifierStub;

/**
 *
 * @author Wamae
 */
@RunWith(Parameterized.class)
public class ParameterizedTests {
    private static TrackingService service = new TrackingService(new NotifierStub());
    private int input;
    private int expected;
    @Parameters
    public static List<Object> data(){
        return Arrays.asList(new Object[][]{
            {5,5},
            {5,10},
            {-12,0},
            {50,50},
            {1,51},
        });
    }

    public ParameterizedTests(int input, int expected) {
        this.input = input;
        this.expected = expected;
    }
    
   @Test
   public void test(){
       if(this.input>0){
           service.addProtein(this.input);
       }else{
           service.removeProtein(-this.input);
       }
       
       assertEquals(expected, service.getTotal());
   }
    
}
